class PostsController < ApplicationController
  def index
    @posts = Post.order(created_at: :asc).page params[:page]
  end

  def show
    @post = Post.find(params[:id])    
    @comments = @post.comments.hash_tree
  end
  
  def create
    @post = Post.new(post_params)
    if @post.save
      flash[:success] = 'Your post was successfully created!'
      redirect_to @post
    else
      render 'new'
    end
  end
  
  private 
  def post_params
    params.require(:post).permit(:subject)
  end
end
