class CommentsController < ApplicationController
  def new
    if params[:parent_id]
      parent = Comment.find(params[:parent_id])
      @comment = Comment.new(parent_id: params[:parent_id],post_id: parent.post_id )
    else
      @comment = Comment.new(post_id: params[:post_id])
    end
  end
    
  def create
    if params[:comment][:parent_id].to_i > 0
       parent = Comment.find_by_id(params[:comment].delete(:parent_id))
       @comment = parent.children.build(comment_params)
     else
       @comment = Comment.new(comment_params)
     end
 
     if @comment.save
       flash[:success] = 'Your comment was successfully added!'
       redirect_to post_path(@comment.post)
     else
       render 'new'
     end
  end
  
  private
  def comment_params
    params.require(:comment).permit(:comment,:post_id)
  end
end
