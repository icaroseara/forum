class Post < ActiveRecord::Base
  validates :subject, presence: true
  has_many :comments, dependent: :destroy
  paginates_per 10
end
