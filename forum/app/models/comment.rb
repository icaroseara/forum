require 'obscenity/active_model'

class Comment < ActiveRecord::Base
  belongs_to :post
  attr_accessor :tree
  acts_as_tree order: 'created_at DESC'
  validates :comment,  obscenity: { sanitize: true, replacement: :vowels }, presence: true
end
