module CommentsHelper
  def comments_tree_for(comments, tree)
    comments.map do |comment, nested_comments|
      comment.tree =  "#{tree}.#{comment.id}"
      render(comment) +
      (nested_comments.size > 0 ? content_tag(:div, comments_tree_for(nested_comments,"#{tree}.#{comment.id}"), class: "replies") : nil)
    end.join.html_safe
  end
end
