# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#  rand(25).times.each do
#    post = Post.create(subject: Faker::Lorem.sentence)
#    rand(5).times.each do
#      Comment.create(comment: Faker::Lorem.paragraph, post: post)
#    end
#  end
